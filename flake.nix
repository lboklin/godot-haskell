{
  description = "A very basic flake";

  outputs = { self, nixpkgs }:
    let
      pkgs = import nixpkgs {
        system = "x86_64-linux";
        overlays = [ self.overlay ];
      };

      hsPkgs = pkgs.haskellPackages;

    in {
      packages.x86_64-linux = {
        inherit (hsPkgs)
          godot-haskell
          godot-haskell-classgen
          godot-haskell-project-generator;
      };

      defaultPackage.x86_64-linux = hsPkgs.godot-haskell;

      apps.x86_64-linux = {
        classgen = {
          type = "app";
          program = "${hsPkgs.godot-haskell-classgen}/bin/godot-haskell-classgen";
        };
        project-generator = {
          type = "app";
          program = "${hsPkgs.godot-haskell-project-generator}/bin/godot-haskell-project-generator";
        };
      };

      defaultApp.x86_64-linux = self.apps.x86_64-linux.project-generator;

      templates.trivial = {
        path = ./template;
        description = "A basic godot-haskell project template";
      };

      defaultTemplate = self.templates.trivial;

      overlay = final: prev: {
        haskellPackages = nixpkgs.legacyPackages."${prev.system}".haskellPackages.override {
          overrides = self: super: {
            godot-haskell-project-generator = super.callPackage ./project-generator/default.nix {};
            godot-haskell-classgen = super.callPackage ./classgen/default.nix {};
            godot-haskell = self.callPackage ./default.nix {};
          };
        };
      };

      devShell.x86_64-linux = pkgs.mkShell {
        buildInputs = with hsPkgs.godot-haskell.getBuildInputs;
          let ghc = hsPkgs.ghcWithHoogle (_: haskellBuildInputs);
          in
            [ ghc pkgs.haskell-language-server pkgs.hlint hsPkgs.cabal-install ] ++
            propagatedBuildInputs ++
            systemBuildInputs;
      };
  };
}
