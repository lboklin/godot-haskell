{ mkDerivation, directory, filepath, fsnotify, hpack, interpolate
, lens, lib
}:
mkDerivation {
  pname = "godot-haskell-project-generator";
  version = "3.3.3.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  libraryToolDepends = [ hpack ];
  executableHaskellDepends = [
    directory filepath fsnotify interpolate lens
  ];
  prePatch = "hpack";
  description = "Godot-Haskell utility for increased type safety";
  license = lib.licenses.bsd3;
}
