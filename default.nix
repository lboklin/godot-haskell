{ mkDerivation, fetchFromGitHub, aeson, ansi-wl-pprint, base, bytestring, c2hs, casing
, colour, containers, directory, extra, filepath, fsnotify, hpack
, interpolate, lens, linear, mtl, parsec, parsers, stm
, template-haskell, text, th-abstraction, unordered-containers
, vector, api-json ? null, lib, godot-haskell-classgen, rsync
}:
let modifyGodotApi =
      lib.optionalString (api-json != null) ''
          cd classgen
          echo "Running godot-haskell-classgen on path " ${api-json}
          ${godot-haskell-classgen}/bin/godot-haskell-classgen ${api-json}
          cd ..
          cp -r src src.bak
          ${rsync}/bin/rsync -a classgen/src/ src/
      '';
    godot-headers-src = fetchFromGitHub {
      owner = "GodotNativeTools";
      repo = "godot_headers";
      rev = "68174528c996b0a598b30a8abe7a7f289df9af90";
      sha256 = "0k45njdp4z9cyqqdyrsfhavjaxk2m50bs8m8bmf35y9y247nnpvx";
    };

in mkDerivation {
  pname = "godot-haskell";
  version = "3.3.3.0";
  src = ./.;

  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    aeson ansi-wl-pprint base bytestring casing colour containers lens
    linear mtl parsec parsers stm template-haskell text
    unordered-containers vector
  ];
  libraryToolDepends = [ c2hs hpack ];
  executableHaskellDepends = [
    aeson base bytestring casing containers directory extra filepath
    fsnotify interpolate lens mtl stm template-haskell text
    th-abstraction unordered-containers vector
  ];

  doHaddock = false;
  preConfigure = ''
    mkdir -p ./godot_headers
    cp -r ${godot-headers-src}/* ./godot_headers/
    hpack
    ${modifyGodotApi}
  '';
  homepage = "https://github.com/KaneTW/godot-haskell#readme";
  description = "Haskell bindings for the Godot game engine API";
  license = lib.licenses.bsd3;
}
