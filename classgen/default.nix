{ mkDerivation, fetchFromGitHub, callCabal2nix, hpack, lib, aeson
, base, bytestring, c2hs, casing, containers , directory, filepath
, haskell-src-exts, lens, mtl, template-haskell, text
, unordered-containers, vector
}:
let haskell-src-exts-qq = callCabal2nix "haskell-src-exts-qq"
      (fetchFromGitHub {
        owner = "KaneTW";
        repo = "haskell-src-exts-qq";
        rev = "a2d9071c9d6a627a253edfaaa64b6b67c9da3534";
        sha256 = "1cvk90zi16m3nnz52gxim9b8sm17356jrp756y95is6ky13l2h60";
      }) {};

    haskell-src-exts-sc = callCabal2nix "haskell-src-exts-sc"
      (fetchFromGitHub {
        owner = "achirkin";
        repo = "haskell-src-exts-sc";
        rev = "cc0e0f8e86814519281805da610d87cc34c86bea";
        sha256 = "17gxxhhflxbqrar0dd32lczs6vz89w8gqg4jv44djp9fxq98xsqq";
      }) {};

in
  mkDerivation {
    pname = "godot-haskell-classgen";
    version = "0.1.0.0";
    src = ./.;
    isLibrary = true;
    isExecutable = true;
    libraryHaskellDepends = [
      aeson base bytestring casing containers haskell-src-exts
      haskell-src-exts-qq haskell-src-exts-sc lens mtl template-haskell
      text unordered-containers vector
    ];
    libraryToolDepends = [ c2hs hpack ];
    executableHaskellDepends = [
      aeson base bytestring casing containers directory filepath
      haskell-src-exts haskell-src-exts-sc lens mtl template-haskell text
      unordered-containers vector
    ];
    executableToolDepends = [ c2hs ];
    prePatch = "hpack";
    homepage = "https://github.com/KaneTW/godot-haskell#readme";
    license = lib.licenses.bsd3;
  }
