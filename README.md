# godot-haskell

Haskell bindings for the Godot game engine.

* Low-level (GDNative) in Godot.Gdnative
* Nativescript (binding classes/methods/etc) in Godot.Nativescript
* High-level (classes generated from the API ) in Godot.Api
* Access methods through Godot.Core

## Getting started

`nix flake new -t gitlab:lboklin/godot-haskell my-project` will give you a basic project. For a demo, see [this dodge-the-creeps example](https://gitlab.com/lboklin/godot-haskell-example).

## Known issues & inconveniences

* Script variables only appear in the editor when you reload it.
* No type safety for call and call_deferred.
* Every time you add a new node which needs a native script you need to manually
  select the library. It's tedious right now. This is the procedure for adding a
  new node backed by Haskell code: create the node, right click it, attach a
  script, select nativescript, the script will open in the editor, in the
  inspector find the Library subheading under NativeScript, click [empty], pick
  Load, the file picker will open, open lib, and select libmyproject.dnlib or
  whatever you've renamed the library to. That's it. Don't edit the empty file
  that's been opened. Now in Haskell, you can create a class with the same name
  as the Godot one and that inherits from the same type. See the demo. There is
  a ticket in Godot to automate this process :(

## [OUTDATED] Changing Godot versions

You will need to regenerate the bindings if you switch Godot versions. At
present, these are generated for the version that corresponds to the
godot_headers submodule included here. This was 3.1 at the time of writing.

To regenerate bindings:

* Replace godot_headers with a version that corresponds to your
  install. Instructions are included there, but that generally means either
  checking out the corresponding files or rebuilding Godot.
* Check out a copy of the godot repo and make sure you're on the commit
  corresponding to your version of godot. You don't need to build this, but you
  probably will anyway. We need it because documentation files are not included
  in the api.json file found in godot_headers.
* Summarize the documentation xml files into a json file:

```bash
# Prerequisite
sudo apt-get install jq libcurl4-gnutls-dev # Or equivalent on you OS/Distro
cabal install xml-to-json

# Generate the JSON
xml-to-json godot-install-directory/doc/classes/*.xml | jq -n '[inputs]' &> godot_doc_classes.json
```

* Build the bindings themselves:

```bash
cd classgen
stack build
rm ../src/Godot/Core/* ../src/Godot/Tools/*
stack exec godot-haskell-classgen -- ../godot_headers/api.json ../godot_doc_classes.json ../
```

That's it! The rest of the bindings are fairly lightweight with few
dependencies, so you shouldn't see much breakage in the rest of the package.

If you want to get an idea of what the Haskell libraries are doing, set the
environment variable `HS_GODOT_DEBUG`.

## Questions

The primary method of contact is the SimulaVR [Discord server](https://discord.gg/V2NgzZt) or the [Matrix space](https://matrix.to/#/#simulavr-space:matrix.org) (there's a bridge between the main channels/rooms + the GitHub repo's gitter).

## Docs

Most of the API is documented but the lowest-level bindings, like GDNative
functions don't have documentation. They are quite intuitive to use though.
